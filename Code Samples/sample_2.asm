bwnot $SP $ZERO     # Initialize the stack pointer to point at the end of RAM

addi $0 $ZERO 10    # $0 = 10
addi $1 $ZERO 100   # $1 = 100

div $2 $1 $0        # $2 = $1 / $0

addi $3 $ZERO 0     # $3 = 0
addi $3 $3 1        # $3 += 1 NOTE: This will increment forever.

subi $PC $PC 2      # $PC -= 2 NOTE: Basically an unconditional jump.

addi $4 $ZERO 10    # $4 = 10 NOTE: This will never execute.