addi $SP $ZERO 1    # $SP = 1
shli $SP $SP 24      # $SP = $SP << 24
addi $SP $SP -1     # $SP += -1
                    # NOTE: The stack pointer is now at the last address of RAM.

smem $SP $SP 0      # M[$SP+0] = $SP

addi $SP $SP -1     # $SP -= 1

addi $1 $ZERO 100   # $1 = 100
divi $1 $1 10       # $1 /= 10
smem $SP $1 1       # M[$SP+1] = $1

mul $1 $1 $1        # $1 = $1 * $1

beq $1 $ZERO 4      # [START OF LOOP] if $1 == 0 skip loop 

smem $SP $1 0       # M[$SP+0] = $1
subi $1 $1 1        # $1 -= 1
subi $SP $SP 1      # $SP -= 1

jmp $PC -5          # [END OF LOOP] jump to [START OF LOOP]

# NOTE: This last part would fill the last 100 Words of RAM with
# numbers from 1 to 100.

lmem $1 $SP 10      # $1 = M[$SP+10] NOTE: This will be equal to 10 because of the above loop.
shli $2 $1 1        # $2 = $1 << 1

bge $1 $2 5         # [START OF LOOP] if $1 >= $2 skip loop

lmem $0 $SP 0       # $0 = M[$SP+0]
addi $0 $0 1        # $0++
smem $SP $0 0       # M[$SP+0] = $0

add $1 $1 $0        # $1 = $1 + $0

jmp $PC -6          # [END OF LOOP] jump to [START OF LOOP]

# NOTE: This part would fill the address right at the stack pointer with the number 4.