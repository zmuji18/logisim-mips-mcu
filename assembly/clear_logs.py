import os

if __name__ == '__main__':

    for dirpath, _, files in os.walk('.'):
        for file in files:
            if file.endswith('.log'):
                os.remove(f'{dirpath}/{file}')