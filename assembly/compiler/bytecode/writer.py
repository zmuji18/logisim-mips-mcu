"""Module used for parsing assembly instructions for the MCU."""


import sys
sys.path.extend(['.', '..', '../..', '../../..'])


import logging

from enum import Enum
from typing import Iterable
from functools import reduce

import assembly.compiler.loggers as loggers

logger = loggers.get_logger(__name__, logging.WARNING)
class OutputFormat(Enum):
    """These enums represent the available output formats for the assembly compiler."""
    
    HEX = 'v3.0 hex'

class Packing(Enum):
    """These enums represent the available byte packing options."""
    
    WORDS = 'words'

class Addressing(Enum):
    """These enums represent the available addressing options."""

    PLAIN = 'plain'

class Endianness(Enum):
    """These enums represent the available endianness options."""
    pass


def generate_bytecode(bytecodes: Iterable[str], output_file: str, output_format: OutputFormat, 
                      packing: Packing=None, addressing: Addressing=None, endianness: Endianness=None) -> None:
    """Converts the passed instruction bytecodes into the specified output format with the given options
    and writes it to an output file with the given name.
    
    ### Parameters
    
    * @bytecodes - An iterable over the instruction bytecodes.
    * @output_file - Path to the output file.
    * @output_format - The format of the generated file.
    * @packing - Specifies how the bytes should be packed within the specified file format.
    * @addressing - Specifies how the bytes should be addressed within the specified file format."""

    with open(output_file, 'w') as output:

        output.write(__get_header(output_format, packing, addressing, endianness))

        for bytecode in bytecodes:
            
            assert __is_binary(bytecode), \
            f'The specified byte code must be a binary number and not {bytecode}.'

            # NOTE Needs to be modified to support other file formats if desired.
            output.write(f'{hex(int(bytecode, 2))[2:].zfill(8)}\n')

def __get_header(output_format: OutputFormat, *additional_options: Enum) -> str:
    """Returns the header for the specified file formatting options."""

    output_format_string = output_format.value
    options_string = " ".join(
        map(
            lambda option: option.value,
            filter(lambda option: option is not None, additional_options)
            )
        )

    header = f'{output_format_string} {options_string}'.strip() 

    return f'{header}\n'

def __is_binary(value: str) -> bool:
    """Returns true if the specified value is a binary number."""

    return value.isdigit() and \
           reduce(lambda so_far, char: so_far and char in {'0', '1'}, value, True)