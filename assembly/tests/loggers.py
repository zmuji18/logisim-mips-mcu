import os
import logging

from datetime import datetime

LOG_FOLDER = './assembly/tests/logs/'

if not os.path.isdir(LOG_FOLDER):
    os.mkdir(LOG_FOLDER)

__loggers: 'dict[str, logging.Logger]' = {}

def get_logger(logger_name: str, level: int) -> logging.Logger:

    if logger_name in __loggers:
        return __loggers[logger_name]

    log_file = f'{LOG_FOLDER}{datetime.now().isoformat().replace(":", "-")} {logger_name}.log'
    handler = logging.FileHandler(log_file, mode='w')

    logger = logging.getLogger(logger_name)
    logger.addHandler(handler)
    logger.setLevel(level)

    __loggers[logger_name] = logger

    return logger