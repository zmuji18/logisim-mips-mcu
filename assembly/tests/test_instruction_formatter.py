import logging

import pytest
import assembly.compiler.instruction.formatter as formatter
import assembly.tests.loggers as loggers

from assembly.tests.fixtures import test_instructions, test_arguments, test_formatted_lines

logger = loggers.get_logger(__name__, logging.INFO)

def test_format_instruction(test_instructions, test_arguments, test_formatted_lines):

    case_id = 1

    for instruction, arguments, formatted_line in zip(test_instructions, test_arguments, test_formatted_lines):
        
        logger.info(f'TC {case_id}:\n'
                    f'\tInstruction: {instruction}\n'
                    f'\tArguments: {arguments}\n'
                    f'\tFormatted Line: {formatted_line}\n')

        if formatted_line == '':
            with pytest.raises(ValueError):
                formatter.format_instruction(instruction, *arguments)
        else:
            assert formatter.format_instruction(instruction, *arguments) == formatted_line

        case_id += 1