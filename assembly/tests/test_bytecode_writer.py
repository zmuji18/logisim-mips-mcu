import pytest
import logging
import os

import assembly.compiler.bytecode.writer as writer
import assembly.tests.loggers as loggers

from assembly.tests.fixtures import test_formatted_lines, test_outputs

logger = loggers.get_logger(__name__, logging.INFO)

def test_generate_bytecode(test_formatted_lines, test_outputs):

    try:
        OUTPUT_FILE = './temp.hex'

        case_id = 1

        for test_formatted_line, test_output in zip(test_formatted_lines, test_outputs):

            logger.info(f'TC {case_id}:\n'
                        f'\tFormatted Line: {test_formatted_line}\n'
                        f'\tOutput: {test_output}\n')
            
            if test_output == '':
                with pytest.raises(AssertionError):
                    writer.generate_bytecode([test_formatted_line], 
                                            OUTPUT_FILE, 
                                            writer.OutputFormat.HEX, 
                                            writer.Packing.WORDS,
                                            writer.Addressing.PLAIN)

            else:
                writer.generate_bytecode([test_formatted_line], 
                                        OUTPUT_FILE, 
                                        writer.OutputFormat.HEX, 
                                        writer.Packing.WORDS,
                                        writer.Addressing.PLAIN)

                with open(OUTPUT_FILE, 'r') as generated_output:
                    
                    lines = list(generated_output)

                    assert len(lines) == 2
                    assert lines[0].strip() == 'v3.0 hex words plain'
                    assert int(lines[1].strip(), 16) == int(test_output, 16)

            case_id += 1

    finally:
        if os.path.isfile(OUTPUT_FILE):
            os.remove(OUTPUT_FILE)
        